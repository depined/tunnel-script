param (
    [string]$AuthToken,
    [ValidateSet("connect", "disconnect")][string]$Status,
    [ValidateSet("tunnel_id", "system_info")][string]$Info = "",  # Added "system_info"
    [int]$CoreCount = 0,         # Default to 0 (no change)
    [string]$Memory = "",        # Default to empty string (no change)
    [string]$SwapSize = ""       # Default to empty string (no change)
)

$unregisterDistro = "Ubuntu-24.04"

# Function to print message in JSON format
function Write-JsonMessage {
    param (
        [string]$command,
        [string]$message
    )
    $jsonObject = @{
        command = $command
        message = $message
    }
    $jsonObject | ConvertTo-Json | Write-Host
}

# Function to create or update the .wslconfig file only if values are passed
function Update-WSLConfig {
    param (
        [int]$cpuLimit,
        [string]$memoryLimit,
        [string]$swapSize
    )


    # Check if at least one value is passed and different from the default
    if ($cpuLimit -gt 0 -or $memoryLimit -ne "" -or $swapSize -ne "") {

        
    # Convert Windows path to WSL path for docker-compose.yml
    $windowsPath = (Get-Location).Path -replace '\\', '/'
    $drive = $windowsPath.Substring(0, 1).ToLower()
    $dockerComposeWslPath = "/mnt/$drive" + $windowsPath.Substring(2) + "/docker-compose.yml"
        
    # Run Docker Compose down
    wsl -d $unregisterDistro -- bash -c "docker-compose -f $dockerComposeWslPath down"

        $wslConfigPath = "$env:USERPROFILE\.wslconfig"
        
        # Initialize the config content
        $configContent = "[wsl2]`n"

        if ($cpuLimit -gt 0) {
            $configContent += "processors=$cpuLimit`n"
        }
        if ($memoryLimit -ne "") {
            $configContent += "memory=$memoryLimit`n"
        }
        if ($swapSize -ne "") {
            $configContent += "swap=$swapSize`n"
        }

        # Create or update the .wslconfig file
        Set-Content -Path $wslConfigPath -Value $configContent
        # Write-Output "WSL config created/updated: $configContent"

        # Restart WSL for config changes to take effect
        # Write-Output "Restarting WSL for config changes to take effect..."
         wsl --shutdown
        # Write-Output "WSL restarted successfully."
    } else {
        # Write-Output "No config parameters were passed, skipping WSL config update."
    }
}

# Function to get system information (max CPU, memory, and swap)
function Get-SystemInfo {
    # Get maximum CPU count
    $cpuCount = (Get-WmiObject -Class Win32_Processor).NumberOfLogicalProcessors

    # Get total physical memory (in GB)
    $memoryInfo = Get-CimInstance -ClassName Win32_ComputerSystem
    $totalMemoryGB = [math]::Round($memoryInfo.TotalPhysicalMemory / 1GB, 2)

    # Get swap size (page file size) in GB
    $swapInfo = Get-CimInstance -ClassName Win32_PageFileUsage
    $swapSizeGB = [math]::Round($swapInfo.AllocatedBaseSize / 1024, 2)

    # Create an object to store the system information
    $systemInfo = @{
        MaxCPU = $cpuCount
        MaxMemoryGB = $totalMemoryGB
        MaxSwapSizeGB = $swapSizeGB
    }

    # Return the system information as JSON
    $systemInfo | ConvertTo-Json | Write-Host
}

# Function to handle the connection logic
function Handle-Connect {
    param (
        [string]$AuthToken
    )

        # Convert Windows path to WSL path for docker-compose.yml
    $windowsPath = (Get-Location).Path -replace '\\', '/'
    $drive = $windowsPath.Substring(0, 1).ToLower()
    $dockerComposeWslPath = "/mnt/$drive" + $windowsPath.Substring(2) + "/docker-compose.yml"

            # Run Docker Compose down
    & wsl -d $unregisterDistro -- bash -c "docker-compose -f $dockerComposeWslPath down"

    # Retrieve motherboard information
    $motherboardInfo = Get-WmiObject -Class Win32_BaseBoard

    # Extract manufacturer, product, and version
    $motherboardManufacturer = $motherboardInfo.Manufacturer
    $motherboardProduct = $motherboardInfo.Product
    $motherboardVersion = $motherboardInfo.Version

    # Detect system type
    $os = Get-CimInstance -ClassName Win32_OperatingSystem
    $systemType = if ($os.Caption -match "Windows") { "windows" } else { "linux" }

    # Get the host IP address
    $hostIP = (Get-NetIPAddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -notmatch "Loopback" } | Select-Object -First 1).IPAddress

    # Set environment variables
    $env:HOST_IP = $hostIP
    $env:AUTH_HEADER = "Bearer $AuthToken"
    $env:API_HOST = "https://api.depined.org"
    $env:MB_MANUFACTURER = $motherboardManufacturer
    $env:MB_PRODUCT = $motherboardProduct
    $env:MB_VERSION = $motherboardVersion
    $env:SYSTEM_TYPE = $systemType

    # Echo environment variables to the console
    Write-Output "HOST_IP: $env:HOST_IP"
    Write-Output "AUTH_HEADER: $env:AUTH_HEADER"
    Write-Output "API_HOST: $env:API_HOST"
    Write-Output "MB_MANUFACTURER: $env:MB_MANUFACTURER"
    Write-Output "MB_PRODUCT: $env:MB_PRODUCT"
    Write-Output "MB_VERSION: $env:MB_VERSION"
    Write-Output "SYSTEM_TYPE: $env:SYSTEM_TYPE"

# Create docker-compose.yml content
$dockerComposeContent = @"
version: '3.8'

services:
  ssh_tunnel:
    image: depined/depined-tunnel:v1
    container_name: ssh_tunnel
    runtime: nvidia
    ports:
      - "65535:22"
    extra_hosts:
      - "host.docker.internal:host-gateway"
    environment:
      - AUTH_HEADER=$env:AUTH_HEADER
      - API_HOST=$env:API_HOST
      - MB_MANUFACTURER=$env:MB_MANUFACTURER
      - MB_PRODUCT=$env:MB_PRODUCT
      - MB_VERSION=$env:MB_VERSION
      - SYSTEM_TYPE=$env:SYSTEM_TYPE
      - AUTH_TOKEN=$AuthToken
    dns:
      - 8.8.8.8
      - 8.8.4.4
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ssh_data:/root/.ssh
      - /dev/mem:/dev/mem
      - depined_shared:/shared
    entrypoint: ['/bin/bash', '-c', 'service ssh start && /root/tunnel.sh && tail -f /dev/null']

  ollama:
    image: ollama/ollama:latest
    container_name: ollama
    deploy:
      resources:
        reservations:
          devices:
            - driver: nvidia
              count: all
              capabilities: [gpu]
    volumes:
      - ollama:/root/.ollama
    ports:
      - "11434:11434"
    networks:
      - ollama_network
    environment:
      - OLLAMA_ORIGINS=* # Allow all origins
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:11434/api/version"]
      interval: 30s
      timeout: 10s
      retries: 5
      start_period: 40s
    restart: unless-stopped

  worker:
    image: depined/scraper-worker:latest
    container_name: celery-worker
    depends_on:
      - ollama
    environment:
      - PYTHONUNBUFFERED=1
      - REDIS_URL=redis://159.223.200.64:6379/0
      - AUTH_TOKEN=$AuthToken
      - API_HOST=https://api.depined.org
    networks:
      - ollama_network
    restart: unless-stopped

volumes:
  ssh_data:
    driver: local
  depined_shared:
    driver: local
  ollama:
    driver: local

networks:
  ollama_network:
    driver: bridge
"@

    # Write the docker-compose.yml content to a file
    $dockerComposeFilePath = "docker-compose.yml"
    Set-Content -Path $dockerComposeFilePath -Value $dockerComposeContent

    # Convert Windows path to WSL path for docker-compose.yml
    $windowsPath = (Get-Location).Path -replace '\\', '/'
    $drive = $windowsPath.Substring(0, 1).ToLower()
    $dockerComposeWslPath = "/mnt/$drive" + $windowsPath.Substring(2) + "/docker-compose.yml"

    # Copy the docker-compose.yml to WSL and run Docker Compose
    & wsl -d $unregisterDistro -- bash -c "cat $dockerComposeWslPath && docker-compose -f $dockerComposeWslPath -p depined up --build"
    
    Write-JsonMessage -command "docker-compose up" -message "Connect successfully"
}

# Function to handle the disconnect logic
function Handle-Disconnect {
    param (
        [string]$AuthToken
    )

    # Convert Windows path to WSL path for docker-compose.yml
    $windowsPath = (Get-Location).Path -replace '\\', '/'
    $drive = $windowsPath.Substring(0, 1).ToLower()
    $dockerComposeWslPath = "/mnt/$drive" + $windowsPath.Substring(2) + "/docker-compose.yml"

    # Get the container ID for ssh_tunnel
    $containerID = & wsl -d $unregisterDistro -- docker ps -qf "name=ssh_tunnel"
    
    if (-not $containerID) {
        Write-JsonMessage -command "docker-compose down" -message "Failed to find ssh_tunnel container"
        exit 1
    }

    # Get the tunnel ID from the container
    $tunnelID = & wsl -d $unregisterDistro -- docker exec $containerID bash -c 'cat /etc/profile | grep TUNNEL_ID | cut -d"=" -f2'
    $tunnelID = $tunnelID.Trim("'")

    if (-not $tunnelID) {
        Write-JsonMessage -command "docker-compose down" -message "Failed to retrieve TUNNEL_ID from container"
        exit 1
    }

    # Run Docker Compose down
    & wsl -d $unregisterDistro -- bash -c "docker-compose -f $dockerComposeWslPath down"

    # Call the API to disconnect the host
    $disconnectPayload = @{
        tunnel_id = $tunnelID
    } | ConvertTo-Json

    $apiResponse = Invoke-RestMethod -Uri "https://api.depined.org/api/host/disconnect" -Method Post -Body $disconnectPayload -ContentType "application/json" -Headers @{ Authorization = "Bearer $AuthToken" }

    if ($apiResponse -eq $null) {
        Write-JsonMessage -command "docker-compose down" -message "Failed to disconnect host via API"
        exit 1
    }
    Write-Output "API Response: $($apiResponse | ConvertTo-Json -Depth 3)"

    Write-JsonMessage -command "docker-compose down" -message "Disconnected and API called successfully"
}

# Main logic
try {
    # Update the .wslconfig file only if at least one argument was passed and restart WSL if needed
    Update-WSLConfig -cpuLimit $CoreCount -memoryLimit $Memory -swapSize $SwapSize

    # Handle tunnel information and system information
    if ($Info -eq "tunnel_id") {
        # Get the container ID for ssh_tunnel
        $containerID = & wsl -d $unregisterDistro -- docker ps -qf "name=ssh_tunnel"
        
        if (-not $containerID) {
            Write-JsonMessage -command "get_tunnel_id" -message "Failed to find ssh_tunnel container"
            exit 1
        }

        # Get the tunnel ID from the container
        $tunnelID = & wsl -d $unregisterDistro -- docker exec $containerID bash -c 'cat /etc/profile | grep TUNNEL_ID | cut -d"=" -f2'
        $tunnelID = $tunnelID.Trim("'")

        if (-not $tunnelID) {
            Write-JsonMessage -command "get_tunnel_id" -message "Failed to retrieve TUNNEL_ID from container"
            exit 1
        }

        # Return the tunnel ID as JSON
        $jsonObject = @{
            command = "get_tunnel_id"
            tunnel_id = $tunnelID
        }
        $jsonObject | ConvertTo-Json | Write-Host
        exit 0
    }
    elseif ($Info -eq "system_info") {
        # Get system information
        Get-SystemInfo
        exit 0
    }

    # Handle connect or disconnect logic based on the Status parameter
    if ($Status -eq "connect") {
        Handle-Connect -AuthToken $AuthToken
    } elseif ($Status -eq "disconnect") {
        Handle-Disconnect -AuthToken $AuthToken
    }

} catch {
    Write-JsonMessage -command "Script Execution" -message "Error: $($_.Exception.Message)"
    exit 1
}

Write-JsonMessage -command "Script Execution" -message "Script completed successfully"
# Write-Output "Connection script executed successfully."
