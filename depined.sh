#!/bin/bash

# Check if AuthToken is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <AuthToken>"
  exit 1
fi

AUTH_TOKEN=$1

# Retrieve motherboard information
MOTHERBOARD_INFO=$(sudo dmidecode -t baseboard)

# Extract manufacturer, product, and version
MOTHERBOARD_MANUFACTURER=$(echo "$MOTHERBOARD_INFO" | grep "Manufacturer:" | awk -F ": " '{print $2}')
MOTHERBOARD_PRODUCT=$(echo "$MOTHERBOARD_INFO" | grep "Product Name:" | awk -F ": " '{print $2}')
MOTHERBOARD_VERSION=$(echo "$MOTHERBOARD_INFO" | grep "Version:" | awk -F ": " '{print $2}')

# Detect system type
if [[ "$(uname)" == "Linux" ]]; then
  SYSTEM_TYPE="linux"
else
  SYSTEM_TYPE="unknown"
fi

# Get the host IP address
HOST_IP=$(hostname -I | awk '{print $1}')

# Create a JSON object to store the information
SYSTEM_INFO=$(jq -n \
                  --arg mb_manufacturer "$MOTHERBOARD_MANUFACTURER" \
                  --arg mb_product "$MOTHERBOARD_PRODUCT" \
                  --arg mb_version "$MOTHERBOARD_VERSION" \
                  --arg system_type "$SYSTEM_TYPE" \
                  --arg host_ip "$HOST_IP" \
                  '{Motherboard_Manufacturer: $mb_manufacturer, Motherboard_Product: $mb_product, Motherboard_Version: $mb_version, System_Type: $system_type, Host_IP: $host_ip}')

# Print the JSON to the console
echo "$SYSTEM_INFO"

# Create docker-compose.yml file
cat <<EOL > docker-compose.yml
version: '3.8'

services:
  ssh_tunnel:
    image: depined/depined-tunnel:v1
    container_name: ssh_tunnel
    runtime: nvidia
    ports:
      - "65535:22"
    extra_hosts:
      - "host.docker.internal:${HOST_IP}"
    environment:
      - AUTH_HEADER=\${AUTH_HEADER}
      - API_HOST=\${API_HOST}
      - MB_MANUFACTURER=\${MB_MANUFACTURER}
      - MB_PRODUCT=\${MB_PRODUCT}
      - MB_VERSION=\${MB_VERSION}
      - SYSTEM_TYPE=\${SYSTEM_TYPE}
      - AUTH_TOKEN=\${AUTH_TOKEN}
    dns:
      - 8.8.8.8
      - 8.8.4.4
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ssh_data:/root/.ssh
      - /dev/mem:/dev/mem
      - depined_shared:/shared

    
    entrypoint: ["/bin/bash", "-c", "service ssh start && /root/tunnel.sh && tail -f /dev/null"]

volumes:
  ssh_data:
    driver: local
  depined_shared:
    driver: local

EOL

# Export environment variables
export HOST_IP=$HOST_IP
export AUTH_HEADER="Bearer $AUTH_TOKEN"
export API_HOST="https://api.depined.org"
export MB_MANUFACTURER=$MOTHERBOARD_MANUFACTURER
export MB_PRODUCT=$MOTHERBOARD_PRODUCT
export MB_VERSION=$MOTHERBOARD_VERSION
export SYSTEM_TYPE=$SYSTEM_TYPE
export AUTH_TOKEN=$AUTH_TOKEN

# Optionally, you can echo the variables to verify
echo "HOST_IP: $HOST_IP"
echo "AUTH_HEADER: $AUTH_HEADER"
echo "API_HOST: $API_HOST"
echo "MB_MANUFACTURER: $MB_MANUFACTURER"
echo "MB_PRODUCT: $MB_PRODUCT"
echo "MB_VERSION: $MB_VERSION"
echo "SYSTEM_TYPE: $SYSTEM_TYPE"
echo "AUTH_TOKEN: $AUTH_TOKEN"

# Run Docker Compose
docker-compose -p depined up --build
