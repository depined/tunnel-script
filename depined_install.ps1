# Define variables
$distro = "Ubuntu2404"
$unregisterDistro = "Ubuntu-24.04"
$destination_path = ".\${distro}.appx"

# Function to display messages
function Display-Message {
    param (
        [string]$command,
        [string]$message
    )
    Write-Host "$command : $message"
}

# Check if the .appx file exists
if (-Not (Test-Path -Path $destination_path)) {
    Display-Message -command "Test-Path -Path $destination_path" -message "File not found: $destination_path"
    exit 1
} else {
    Display-Message -command "Test-Path -Path $destination_path" -message "File found: $destination_path"
}

try {
    # Install the .appx package
    Add-AppxPackage -Path $destination_path
    Display-Message -command "Add-AppxPackage -Path $destination_path" -message "Installed package"
    
    # Unregister the distro if it exists
    try {
        wsl --unregister $unregisterDistro 2>$null
        Display-Message -command "wsl --unregister $unregisterDistro" -message "Unregistered distro"
    } catch {
        Display-Message -command "wsl --unregister $unregisterDistro" -message "Distro not found, continuing"
    }
    
    # Install the distro
    & $distro install --root
    Display-Message -command "$distro install --root" -message "Installed distro"

    # Create the installation script content
    $installScript = @'
#!/bin/bash
set -e
apt-get update -y
DEBIAN_FRONTEND=noninteractive apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update -y
DEBIAN_FRONTEND=noninteractive apt-get install -y docker-ce docker-ce-cli containerd.io

curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
  && curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
    sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
    sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

  sudo apt-get update -y 
  sudo apt-get install -y nvidia-container-toolkit

# Create the daemon.json content
echo '{
  "runtimes": {
    "nvidia": {
      "path": "nvidia-container-runtime",
      "runtimeArgs": []
    }
  }
}' > /etc/docker/daemon.json

systemctl restart docker

curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
systemctl start docker
systemctl enable docker
echo "Docker and Docker Compose installation completed."
'@

    Display-Message -command "Bash Script Preparation" -message "Bash script created"

    # Convert Windows path to WSL path manually
    $windowsPath = (Get-Location).Path -replace '\\', '/'
    $drive = $windowsPath.Substring(0, 1).ToLower()
    $wslPath = "/mnt/$drive" + $windowsPath.Substring(2) + "/install_docker.sh"

    # Execute the script in WSL as root
    $installScript -replace "`r`n", "`n" | & wsl -d $unregisterDistro -- bash -c "cat > $wslPath && bash $wslPath && rm $wslPath"
    
    if ($LASTEXITCODE -ne 0) {
        throw "Docker installation script failed with exit code $LASTEXITCODE"
    }

    Display-Message -command "Docker Installation Script" -message "Docker and Docker Compose installation completed."

} catch {
    Display-Message -command "Installation process" -message "Error: $($_.Exception.Message)"
    exit 1
}

Display-Message -command "Script Execution" -message "Script completed successfully"
Write-Output "Docker and Docker Compose installation completed."
