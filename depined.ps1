param (
    [string]$AuthToken
)

# Check if AuthToken is provided
if (-not $AuthToken) {
    Write-Error "AuthToken is required. Usage: .\run.ps1 -AuthToken <your_token>"
    exit 1
}

# Retrieve motherboard information
$motherboardInfo = Get-WmiObject -Class Win32_BaseBoard

# Extract manufacturer, product, and version
$motherboardManufacturer = $motherboardInfo.Manufacturer
$motherboardProduct = $motherboardInfo.Product
$motherboardVersion = $motherboardInfo.Version

# Detect system type
$os = Get-CimInstance -ClassName Win32_OperatingSystem
$systemType = if ($os.Caption -match "Windows") { "windows" } else { "linux" }

# Get the host IP address
$hostIP = (Get-NetIPAddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -notmatch "Loopback" } | Select-Object -First 1).IPAddress

# Create an object to store the information
$systemInfo = [PSCustomObject]@{
    Motherboard_Manufacturer = $motherboardManufacturer
    Motherboard_Product = $motherboardProduct
    Motherboard_Version = $motherboardVersion
    System_Type = $systemType
    Host_IP = $hostIP
}

# Convert the object to JSON
$systemInfoJson = $systemInfo | ConvertTo-Json -Depth 3

# Print the JSON to the console
Write-Output $systemInfoJson

# Set environment variables
$env:HOST_IP = $hostIP
$env:AUTH_HEADER = "Bearer $AuthToken"
$env:API_HOST = "https://api.depined.org"
$env:MB_MANUFACTURER = $motherboardManufacturer
$env:MB_PRODUCT = $motherboardProduct
$env:MB_VERSION = $motherboardVersion
$env:SYSTEM_TYPE = $systemType

# Echo environment variables to the console
Write-Output "HOST_IP: $env:HOST_IP"
Write-Output "AUTH_HEADER: $env:AUTH_HEADER"
Write-Output "API_HOST: $env:API_HOST"
Write-Output "MB_MANUFACTURER: $env:MB_MANUFACTURER"
Write-Output "MB_PRODUCT: $env:MB_PRODUCT"
Write-Output "MB_VERSION: $env:MB_VERSION"
Write-Output "SYSTEM_TYPE: $env:SYSTEM_TYPE"

# Create docker-compose.yml content
$dockerComposeContent = @"
version: '3.8'

services:
  ssh_tunnel:
    image: depined/depined-tunnel:v1
    container_name: ssh_tunnel
    runtime: nvidia
    ports:
      - "65535:22"
    extra_hosts:
      - "host.docker.internal:$env:HOST_IP"
    environment:
      - AUTH_HEADER=$env:AUTH_HEADER
      - API_HOST=$env:API_HOST
      - MB_MANUFACTURER=$env:MB_MANUFACTURER
      - MB_PRODUCT=$env:MB_PRODUCT
      - MB_VERSION=$env:MB_VERSION
      - SYSTEM_TYPE=$env:SYSTEM_TYPE
      - AUTH_TOKEN=$AuthToken
    dns:
      - 8.8.8.8
      - 8.8.4.4
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ssh_data:/root/.ssh
      - /dev/mem:/dev/mem
      - depined_shared:/shared
    entrypoint: ["/bin/bash", "-c", "service ssh start && /root/tunnel.sh && tail -f /dev/null"]

volumes:
  ssh_data:
    driver: local
  depined_shared:
    driver: local
"@

# Write the docker-compose.yml content to a file
$dockerComposeContent | Out-File -FilePath "docker-compose.yml" -Encoding UTF8

# Run Docker Compose
docker-compose -f docker-compose.yml -p depined up  --build
