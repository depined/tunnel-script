#!/bin/sh

# Create the log file if it does not exist and start logging
touch /var/log/container_init.log
echo "Logging initialized" >> /var/log/container_init.log

# Log each command before executing
set -x

# Check if the script is already running with bash
if [ -z "$BASH_VERSION" ]; then
    # Log the start of bash detection and installation process
    echo "Checking if bash is available and installing if not present" >> /var/log/container_init.log

    # Detect the OS type and install bash if not already present
    if [ -f /etc/alpine-release ]; then
        # Alpine Linux
        echo "Detected Alpine Linux" >> /var/log/container_init.log
        apk update >> /var/log/container_init.log 2>&1 && apk add --no-cache bash >> /var/log/container_init.log 2>&1
    elif [ -f /etc/debian_version ]; then
        # Debian/Ubuntu
        echo "Detected Debian/Ubuntu" >> /var/log/container_init.log
        apt-get update >> /var/log/container_init.log 2>&1 && apt-get install -y bash >> /var/log/container_init.log 2>&1
    elif [ -f /etc/redhat-release ]; then
        if grep -q 'Fedora' /etc/redhat-release; then
            # Fedora
            echo "Detected Fedora" >> /var/log/container_init.log
            dnf update -y >> /var/log/container_init.log 2>&1 && dnf install -y bash >> /var/log/container_init.log 2>&1
        else
            # RHEL/CentOS
            echo "Detected RHEL/CentOS" >> /var/log/container_init.log
            yum update -y >> /var/log/container_init.log 2>&1 && yum install -y bash >> /var/log/container_init.log 2>&1
        fi
    else
        echo "Unsupported OS" >> /var/log/container_init.log
        exit 1
    fi

    # Log the re-execution with bash
    echo "Re-executing the script with bash" >> /var/log/container_init.log
    exec /bin/bash "$0" "$@"
fi

# The rest of the script runs with bash

# Redirect output to log file and console
exec > >(tee -a /var/log/container_init.log) 2>&1

# Log each command before executing
set -x

# Detect the OS type and install the necessary packages
if [ -f /etc/alpine-release ]; then
    # Alpine Linux
    echo "Detected Alpine Linux"
    apk update && apk add --no-cache openssh python3 py3-pip openrc gcc musl-dev linux-headers python3-dev
elif [ -f /etc/debian_version ]; then
    # Debian/Ubuntu
    echo "Detected Debian/Ubuntu"
    apt-get update && apt-get install -y openssh-server python3-pip python3-venv build-essential python3-dev
elif [ -f /etc/redhat-release ]; then
    if grep -q 'Fedora' /etc/redhat-release; then
        # Fedora
        echo "Detected Fedora"
        dnf update -y && dnf install -y openssh-server python3-pip gcc gcc-c++ kernel-devel python3-devel
    else
        # RHEL/CentOS
        echo "Detected RHEL/CentOS"
        yum update -y && yum install -y openssh-server python3-pip gcc gcc-c++ kernel-devel python3-devel
    fi
else
    echo "Unsupported OS"
    exit 1
fi

# Upgrade pip
pip install --upgrade pip

# Create a Python virtual environment and install JupyterLab
python3 -m venv /opt/venv && . /opt/venv/bin/activate && pip install wheel && pip install jupyterlab

# Debugging: Verify JupyterLab installation
echo "JupyterLab installation complete. Verifying..."
/opt/venv/bin/jupyter --version

# Configure SSH
mkdir -p /root/.ssh && chmod 700 /root/.ssh && echo "$SSH_PUBLIC_KEY" > /root/.ssh/authorized_keys && chmod 600 /root/.ssh/authorized_keys
echo "PermitRootLogin without-password" >> /etc/ssh/sshd_config && echo "PasswordAuthentication no" >> /etc/ssh/sshd_config

# Start SSH and JupyterLab
if command -v rc-service >/dev/null 2>&1; then
    rc-service sshd start
else
    service ssh start
fi

# Debugging: Verify JupyterLab command
echo "Starting JupyterLab..."
/opt/venv/bin/jupyter lab --allow-root --ip=0.0.0.0 --port=8888 --no-browser --NotebookApp.token='' --NotebookApp.password='' &
tail -f /dev/null
